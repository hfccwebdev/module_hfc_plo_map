<?php

namespace Drupal\hfc_plo_map\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Program Learning Outcomes grid form.
 *
 * @package Drupal\hfc_plo_map\Form
 */
class PloGridForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'plo_grid_edit_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $outcomes = NULL, $courses = NULL, $values = NULL) {

    $form['original_values'] = [
      '#type' => 'value',
      '#value' => $values,
    ];

    $form['plo_grid'] = [
      '#tree' => TRUE,
      '#prefix' => '<table>',
      '#suffix' => '</table',
    ];

    $form['plo_grid']['header'] = [
      '#prefix' => '<tr><th>Outcome</th><th>',
      '#markup' => implode('</th><th>', $courses),
      '#suffix' => '</th></tr>',
    ];

    foreach ($outcomes as $row => $outcome) {
      $form['plo_grid'][$row] = [
        '#prefix' => '<tr>',
        '#suffix' => '</tr>',
      ];
      $form['plo_grid'][$row]['outcome'] = [
        '#prefix' => '<td>',
        '#markup' => t($outcome),
        '#suffix' => '</td>',
      ];
      foreach ($courses as $course => $name) {
        $form['plo_grid'][$row][$course] = [
        '#prefix' => '<td>',
          '#type' => 'checkboxes',
          '#options' => ['I' => 'I', 'D' => 'D', 'C' => 'C'],
          // '#default_value' => !empty($values[$row][$course]) ? $values[$row][$course] : NULL,
          '#suffix' => '</td>',
        ];
      }
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // $master = $form_state->getValue('master');
    // $catalog = $form_state->getValue('catalog');

  }
}
