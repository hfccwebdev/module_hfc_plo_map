<?php

namespace Drupal\hfc_plo_map\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\Entity\Node;

/**
 * Class PloEditor.
 *
 * @package Drupal\hfc_plo_map\Controller
 */
class PloEditor extends ControllerBase {

  /**
   * Checks access for a specific request.
   *
   * @param AccountInterface $account
   *   Run access checks for this account.
   * @param Node $node
   *   Run custom access checks for this node.
   *
   * @return AccessResult
   *   The access result.
   */
  public function access(AccountInterface $account, Node $node) {
    // Check permissions and combine that with any custom access checking needed. Pass forward
    // parameters from the route and/or request as needed.

    if ($node->getType() == 'program_learning_outcome') {
      // @todo: There's got to be a better way...
      return AccessResult::allowedIf(
        ($account->hasPermission('bypass node access') ||
        $account->hasPermission('edit any program_learning_outcome content')) &&
        $node->field_program_master->target_id
      );
    }
    else {
      return AccessResult::forbidden();
    }
  }

  /**
   * Edit an eligible node.
   *
   * Access control check should have already verified eligibility.
   *
   * @see PloEditor::access().
   *
   * @param Node $node
   *   The source node.
   *
   * @return \Drupal\Core\Form\FormInterface
   *   Confirmation form.
   */
  public function build(Node $node) {

    $program_master = $node->field_program_master->entity;
    $master_outcomes = $program_master->field_program_outcomes_old->getValue();

    // For now, $master_outcomes is just a text field, which was intended to be temporary until this system could be built.
    // However, integrating PLOs into the proposal system would probably be a nightmare, so maybe it'll always be that way.
    // Or else we can now simply migrate it to an MV text-plain field. In any case, for now, parse it.

    $outcomes = [];
    foreach (explode(PHP_EOL, $master_outcomes[0]['value']) as $outcome) {
      $outcomes[] = trim(preg_replace('/^\* /', '', $outcome));
    }

    $courses = [];

    $courses = $this->getCourseNames($program_master->get('field_program_rqcor_list')->referencedEntities());
    $courses += $this->getCourseNames($program_master->get('field_program_rqsup_list')->referencedEntities());
    // Do we need this???
    // $courses += $this->getCourseNames($program_master->field_program_admission_list->getValue());

    asort($courses);

    // Parse $node->field_outcomes->getValue() into an array that can provide default values.
    $values = [];

    return \Drupal::formBuilder()->getForm('Drupal\hfc_plo_map\Form\PloGridForm', $outcomes, $courses, $values);

  }

  /**
   * Get course names from list of required courses.
   */
  private function getCourseNames($courses) {
    $names = [];
    foreach ($courses as $rqc) {
      if ($course_master = $rqc->getMaster()) {
        if ($course_master->getType() == 'course_master') {
          $names[$rqc->id()] = $course_master->get('field_crs_subject')->target_id . '-' . $course_master->get('field_crs_number')->value;
        }
        elseif ($course_master->getType() == 'pseudo_course') {
          $names[$rqc->id()] = $course_master->label();
        }
        else {
          drupal_set_message(t('Cannot process unknown course master for RQC #@id: @title!', ['@id' => $rqc->id(), '@title' => $rqc->label()]), 'error');
        }
      }
      else {
        drupal_set_message(t('Cannot retrieve Course Master for RQC #@id: @title!', ['@id' => $rqc->id(), '@title' => $rqc->label()]), 'error');
      }
    }
    return $names;
  }
}
